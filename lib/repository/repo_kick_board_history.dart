import 'package:daligo_customer_app/model/common_result.dart';
import 'package:daligo_customer_app/model/ing_kick_board_result.dart';
import 'package:daligo_customer_app/model/kick_board_use_request.dart';
import 'package:daligo_customer_app/model/my_history_result.dart';
import 'package:dio/dio.dart';
import 'package:daligo_customer_app/config/config_api.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/model/my_history_list_result.dart';

class RepoKickBoardHistory {
  // 다트문법. 페이지 언급안하면 기본값 1 주고싶을때.. {} 여기 안에 이렇게 쓰면 됨.
  // 자바에선 안됨.. 다트가 더 늦게 나온 언어라서 기존 언어 단점이 보완된거임..불편하니까..
  Future<MyHistoryListResult> getList({int page = 1}) async {
    final String _baseUrl = '$apiUri/kick-board-history/histories?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MyHistoryListResult.fromJson(response.data);
  }

  Future<MyHistoryResult> getHistory(int id) async {
    final String _baseUrl = '$apiUri/kick-board-history/history?id={id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MyHistoryResult.fromJson(response.data);
  }

  Future<IngKickBoardResult> getCurrentUsing() async {
    const String _baseUrl = '$apiUri/kick-board-history/ing';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return IngKickBoardResult.fromJson(response.data);
  }

  Future<CommonResult> setStart(int kickBoardId, KickBoardUseRequest request) async {
    const String _baseUrl = '$apiUri/kick-board-history/kick-board-id/{kickBoardId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        _baseUrl.replaceAll('{kickBoardId}', kickBoardId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> putEnd(int historyId, KickBoardUseRequest request) async {
    const String _baseUrl = '$apiUri/kick-board-history/kick-board-id/{kickBoardId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        _baseUrl.replaceAll('{kickBoardId}', historyId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}