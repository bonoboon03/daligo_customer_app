import 'package:daligo_customer_app/config/config_api.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/model/near_kick_board_list_result.dart';
import 'package:dio/dio.dart';

class RepoKickBoard {
  Future<NearKickBoardListResult> getList(double posX, double posY) async {
    const String _baseUrl = '$apiUri/kick-board/near?distanceKm=1&posX={posX}&posY={posY}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl.replaceAll('{posX}', posX.toString()).replaceAll('{posY}', posY.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NearKickBoardListResult.fromJson(response.data);

  }
}