class FindUsernameResponse {
  String username;

  FindUsernameResponse(this.username);

  factory FindUsernameResponse.fromJson(Map<String, dynamic> json) {
    return FindUsernameResponse(
        json['username'],
    );
  }
}