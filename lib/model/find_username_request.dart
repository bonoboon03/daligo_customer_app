class FindUsernameRequest {
  String name;
  String phoneNumber;

  FindUsernameRequest(this.name, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['name'] = this.name;
    data['phoneNumber'] = this.phoneNumber;

    return data;
  }
}