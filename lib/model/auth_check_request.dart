class AuthCheckRequest {
  String phoneNumber;
  String authNumber;

  AuthCheckRequest(this.phoneNumber, this.authNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['phoneNumber'] = this.phoneNumber;
    data['authNumber'] = this.authNumber;

    return data;
  }
}