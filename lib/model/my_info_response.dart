class MyInfoResponse {
  String name;
  String phoneNumber;
  num price;
  String dateBirth;

  MyInfoResponse(this.name, this.phoneNumber, this.price, this.dateBirth);

  factory MyInfoResponse.fromJson(Map<String, dynamic> json) {
    return MyInfoResponse(
      json['name'],
      json['phoneNumber'],
      json['price'],
      json['dateBirth'],
    );
  }
}