import 'package:daligo_customer_app/model/ing_kick_board_response.dart';

class IngKickBoardResult {
  IngKickBoardResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  IngKickBoardResult(this.data, this.isSuccess, this.code, this.msg);

  factory IngKickBoardResult.fromJson(Map<String, dynamic> json) {
    return IngKickBoardResult(
        IngKickBoardResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}