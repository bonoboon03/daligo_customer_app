class MyHistoryResponse {
  String kickBoard;
  String dateStart;
  String dateEnd;
  String timeUse;
  num basePrice;
  num perMinPrice;
  num resultPrice;

  MyHistoryResponse(this.kickBoard, this.dateStart, this.dateEnd, this.timeUse, this.basePrice, this.perMinPrice, this.resultPrice);

  factory MyHistoryResponse.fromJson(Map<String, dynamic> json) {
    return MyHistoryResponse(
        json['kickBoard'],
        json['dateStart'],
        json['dateEnd'],
        json['timeUse'],
        json['basePrice'],
        json['perMinPrice'],
        json['resultPrice'],
    );
  }
}