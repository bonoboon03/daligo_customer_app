class FindPasswordRequest {
  String name;
  String username;
  String phoneNumber;

  FindPasswordRequest(this.name, this.username, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['username'] = username;
    data['phoneNumber'] = phoneNumber;

    return data;
  }
}