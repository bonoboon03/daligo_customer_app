class MyHistoryItem {
  int id;
  String kickBoard;
  String dateStart;
  String timeUse;
  num resultPrice;

  MyHistoryItem(this.id, this.kickBoard, this.dateStart, this.timeUse, this.resultPrice);

  factory MyHistoryItem.fromJson(Map<String, dynamic> json) {
    return MyHistoryItem(
      json['id'],
      json['kickBoard'],
      json['dateStart'],
      json['timeUse'],
      json['resultPrice'],
    );
  }
}