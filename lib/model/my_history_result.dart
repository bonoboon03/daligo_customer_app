import 'package:daligo_customer_app/model/my_history_response.dart';

class MyHistoryResult {
  MyHistoryResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  MyHistoryResult(this.data, this.isSuccess, this.code, this.msg);

  factory MyHistoryResult.fromJson(Map<String, dynamic> json) {
    return MyHistoryResult(
        MyHistoryResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}