class MemberJoinRequest {
  String name;
  String username;
  String password;
  String passwordRe;
  String phoneNumber;
  String licenseNumber;
  String dateBirth;

  MemberJoinRequest(this.name, this.username, this.password, this.passwordRe, this.phoneNumber, this.licenseNumber, this.dateBirth);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['username'] = username;
    data['password'] = password;
    data['passwordRe'] = passwordRe;
    data['phoneNumber'] = phoneNumber;
    data['licenseNumber'] = licenseNumber;
    data['dateBirth'] = dateBirth;

    return data;
  }
}