class IngKickBoardResponse {
  int historyId;
  String uniqueNumber;
  double startPosX;
  double startPosY;
  String dateStart;
  String priceBasis;
  num basePrice;
  num perMinPrice;

  IngKickBoardResponse(this.historyId, this.uniqueNumber, this.startPosX, this.startPosY, this.dateStart, this.priceBasis, this.basePrice, this.perMinPrice);

  factory IngKickBoardResponse.fromJson(Map<String, dynamic> json) {
    return IngKickBoardResponse(
      json['historyId'],
      json['uniqueNumber'],
      json['startPosX'],
      json['startPosY'],
      json['dateStart'],
      json['priceBasis'],
      json['basePrice'],
      json['perMinPrice'],
    );
  }
}