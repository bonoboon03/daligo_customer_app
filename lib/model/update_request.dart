class UpdateRequest {
  String name;
  String phoneNumber;
  String licenseNumber;

  UpdateRequest(this.name, this.phoneNumber, this.licenseNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['licenseNumber'] = licenseNumber;

    return data;
  }
}