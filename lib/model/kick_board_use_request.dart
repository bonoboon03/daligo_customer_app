class KickBoardUseRequest {
  double posX;
  double posY;

  KickBoardUseRequest(this.posX, this.posY);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['posX'] = posX;
    data['posY'] = posY;

    return data;
  }
}