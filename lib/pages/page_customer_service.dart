import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_text_icon_full_btn.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:flutter/material.dart';

class PageCustomerService extends StatefulWidget {
  const PageCustomerService({super.key});

  @override
  State<PageCustomerService> createState() => _PageCustomerServiceState();
}

class _PageCustomerServiceState extends State<PageCustomerService> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: "고객센터"),
      body: Container(
        padding: const EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "고객센터 상담시간",
              style: TextStyle(
                fontSize: fontSizeSuper,
              ),
            ),
            const ComponentMarginVertical(),
            const Divider(color: Colors.black),
            const ComponentMarginVertical(),
            const Text(
              "전화상담 : 평일 09:00 - 18:00",
              style: TextStyle(
                fontSize: fontSizeBig,
              ),
            ),
            const ComponentMarginVertical(),
            const Text(
              "점심시간 12:00 - 14:00 / 주말 공휴일 휴무",
              style: TextStyle(
                fontSize: 10,
                color: colorDarkGray,
              ),
            ),
            const ComponentMarginVertical(),
            const Text(
              "채팅상담 : 평일 09:30 - 익일 01:00",
              style: TextStyle(
                fontSize: fontSizeBig,
              ),
            ),
            const ComponentMarginVertical(),
            const Text(
              "점심시간 12:00 - 14:00 / 공휴일 휴무",
              style: TextStyle(
                fontSize: 10,
                color: colorDarkGray,
              ),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.big),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextIconFullBtn(
                  Colors.amber, Colors.white, Icons.message, '채팅 상담하기', () {}),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextIconFullBtn(
                  Colors.green, Colors.white, Icons.call, '전화 상담하기', () {}),
            ),
          ],
        ),
      ),
      drawer: const ComponentDrawer(),
    );
  }
}
