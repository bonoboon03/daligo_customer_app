import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/config/config_style.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/pages/page_find_password.dart';
import 'package:daligo_customer_app/pages/page_login.dart';
import 'package:flutter/material.dart';

class PageCheckUsername extends StatefulWidget {
  const PageCheckUsername({super.key, required this.username});

  final String username;

  @override
  State<PageCheckUsername> createState() => _PageCheckUsernameState();
}

class _PageCheckUsernameState extends State<PageCheckUsername> {
  String username = "";

  Future<void> _checkUsername() async {
    setState(() {
      username = widget.username;
    });
  }

  @override
  void initState() {
    super.initState();
    _checkUsername();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '아이디 확인',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            top: 20,
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    const Text(
                      "회원님의 정보와 일치하는 아이디입니다.",
                      style: TextStyle(
                        fontSize: fontSizeBig,
                      ),
                    ),
                    Text(
                      username,
                      style: const TextStyle(
                        fontSize: fontSizeBig,
                      ),
                    ),
                  ],
                ),
              ),
              const ComponentMarginVertical(
                enumSize: EnumSize.big,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.44,
                    child: ElevatedButton(
                      onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => const PageLogin())),
                      style: ElevatedButton.styleFrom(
                        primary: colorPrimary,
                        onPrimary: Colors.white,
                        padding: contentPaddingButton,
                        elevation: buttonElevation,
                        textStyle: const TextStyle(
                          fontSize: fontSizeMid,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(buttonRadius),
                          side: const BorderSide(color: colorPrimary),
                        ),
                      ),
                      child: const Text("로그인하기"),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.44,
                    child: ElevatedButton(
                      onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => const PageFindPassword())),
                      style: ElevatedButton.styleFrom(
                        primary: colorPrimary,
                        onPrimary: Colors.white,
                        padding: contentPaddingButton,
                        elevation: buttonElevation,
                        textStyle: const TextStyle(
                          fontSize: fontSizeMid,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(buttonRadius),
                          side: const BorderSide(color: colorPrimary),
                        ),
                      ),
                      child: const Text("비밀번호 찾기"),
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(
                enumSize: EnumSize.small,
              ),
            ],
          ),
        ),
      ],
    );
  }
}