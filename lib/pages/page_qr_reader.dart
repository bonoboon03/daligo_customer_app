import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/model/kick_board_use_request.dart';
import 'package:daligo_customer_app/pages/page_ing_kick_board.dart';
import 'package:daligo_customer_app/repository/repo_kick_board_history.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_dart_scan/qr_code_dart_scan.dart';

class PageQrReader extends StatefulWidget {
  const PageQrReader({super.key, required this.posX, required this.posY});

  final double posX;
  final double posY;

  @override
  State<PageQrReader> createState() => _PageQrReaderState();
}

class _PageQrReaderState extends State<PageQrReader> {
  Result? currentResult;

  Future<void> _setStart(int kickBoardId, KickBoardUseRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(
        cancelFunc: cancelFunc,
      );
    });

    await RepoKickBoardHistory()
        .setStart(kickBoardId, request)
        .then((res) => {BotToast.closeAllLoading(), setState(() {})})
        .catchError((err) => {
              BotToast.closeAllLoading(),
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: '킥보드 QR코드 스캔'),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (currentResult?.text != null) {
      return Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width, // 사진 꽉 차게
              height: 200,
              color: Colors.white,
              child: Image.asset('assets/login_bg.jpg', fit: BoxFit.cover), // 비율이 변경되지 않고 꽉 채움. 이미지 잘릴 수 있음.
            ),
            const ComponentMarginVertical(enumSize: EnumSize.big),
            Center(
              child: Text(
                "킥보드를 사용하시려면 사용 시작 버튼을 눌러주세요.",
                style: TextStyle(
                  fontSize: fontSizeBig
                ),
              ),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                '사용 시작',
                () {
                  KickBoardUseRequest kickBoardUseRequest =
                      KickBoardUseRequest(widget.posX, widget.posY);
                  _setStart(int.parse(currentResult!.text), kickBoardUseRequest);
                  Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) => PageIngKickBoard(id: int.parse(currentResult!.text))));
                },
              ),
            ),
          ],
        ),
      );
    } else {
      return QRCodeDartScanView(
        scanInvertedQRCode: true,
        onCapture: (Result result) {
          setState(() {
            currentResult = result;
          });
        },
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(currentResult?.text ?? '킥보드 상단 QR코드를 찍으세요'),
              ],
            ),
          ),
        ),
      );
    }
  }
}
