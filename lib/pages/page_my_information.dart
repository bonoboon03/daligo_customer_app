import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_response.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/components/common/component_text_icon_full_btn.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/model/my_info_result.dart';
import 'package:daligo_customer_app/pages/page_member_update.dart';
import 'package:daligo_customer_app/pages/page_password_update.dart';
import 'package:daligo_customer_app/pages/page_charging_price.dart';
import 'package:daligo_customer_app/repository/repo_member.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageMyInformation extends StatefulWidget {
  const PageMyInformation({super.key});

  @override
  State<PageMyInformation> createState() => _PageMyInformationState();
}

class _PageMyInformationState extends State<PageMyInformation> {
  String name = "";
  String phoneNumber = "";
  num price = 0;
  String dateBirth = "";

  var maskNumFormatter = NumberFormat('###,###,###,###');

  Future<void> _getMyInfo() async {
    MyInfoResult result = await RepoMember().getMyInfo();

    setState(() {
      name = result.data.name;
      phoneNumber = result.data.phoneNumber;
      price = result.data.price;
      dateBirth = result.data.dateBirth;
    });
  }

  Future<void> _logout() async {
    TokenLib.logout(context);
  }

  @override
  void initState() {
    super.initState();
    _getMyInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: "내 정보"),
      body: _buildBody(context),
      drawer: const ComponentDrawer(),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: colorLightGray,
                  ),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Column(
                  children: [
                    ComponentResponse(title: '이름', content: name),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    ComponentResponse(title: '연락처', content: phoneNumber),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    ComponentResponse(title: '보유금액', content: '${maskNumFormatter.format(price)}원'),
                    const ComponentMarginVertical(enumSize: EnumSize.mid),
                    ComponentResponse(title: '생년월일', content: dateBirth),
                  ],
                ),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextIconFullBtn(Colors.blue, Colors.white, Icons.lock, '로그아웃', () { _showLogoutDialog(); }),
              ),
              const ComponentMarginVertical(),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextIconFullBtn(Colors.blue, Colors.white, Icons.restart_alt, '회원 수정', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberUpdate()))),
              ),
              const ComponentMarginVertical(),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextIconFullBtn(Colors.blue, Colors.white, Icons.lock_open, '비밀번호 수정', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PagePasswordUpdate()))),
              ),
            ],
          ),
        ),
      ],
    );
  }
  void _showLogoutDialog() {
    showDialog(
      context: context,
      barrierDismissible: false, // 외부 화면을 눌러도 Dialog가 꺼지지 않게 함
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('로그아웃'),
          content: const Text('정말 로그아웃 하시겠습니까?'),
          actions: [
            ComponentTextBtn('확인',
                  () async { _logout(); },
            ),
            ComponentTextBtn('취소',
                  () { Navigator.of(context).pop(); },
              bgColor: Colors.black,
              borderColor: Colors.black,
            ),
          ],
        );
      },
    );
  }
}