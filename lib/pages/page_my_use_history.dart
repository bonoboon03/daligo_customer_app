import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/components/common/component_list_textline_item.dart';
import 'package:daligo_customer_app/pages/page_my_use_history_detail.dart';
import 'package:flutter/material.dart';
import 'package:daligo_customer_app/components/common/component_count_title.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_no_contents.dart';
import 'package:daligo_customer_app/model/my_history_item.dart';
import 'package:daligo_customer_app/repository/repo_kick_board_history.dart';
import 'package:intl/intl.dart';

class PageMyUseHistory extends StatefulWidget {
  const PageMyUseHistory({super.key});

  @override
  State<PageMyUseHistory> createState() => _PageMyUseHistoryState();
}

class _PageMyUseHistoryState extends State<PageMyUseHistory> {
  /*
  * 인피니티 스크롤 구현할거임.
  * 그래서 스크롤을 컨트롤해야함..
  * 모바일에서는 페이징할때 버튼으로 하면 힘드니까
  * 페이지 바닥 찍으면 다음페이지 더 추가해서 보고..
  * 이런식으로 해야함..
  * 구글에 인피니티 스크롤 검색해보기.
  * */
  final _scrollController = ScrollController();

  var maskNumFormatter = NumberFormat('###,###,###,###');

  List<MyHistoryItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoKickBoardHistory()
          .getList(page: _currentPage)
          .then((res) => {
        BotToast.closeAllLoading(),
        setState(() {
          _totalItemCount = res.totalItemCount;
          _totalPage = res.totalPage;
          _list = [..._list, ...?res.list]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
          _currentPage++;
        })
      }).catchError((err) => {
        BotToast.closeAllLoading(),
        print(err),
      });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: "나의 이용기록",
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(context),
        ],
      ),
      drawer: const ComponentDrawer(),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_totalItemCount > 0) {
      return Container(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ComponentCountTitle(Icons.attachment, _totalItemCount, '건', '이용내역'),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) => ComponentListTextLineItem(
                title: _list[index].kickBoard,
                isUseContent1Line: true,
                content1Subject: '이용시작',
                content1Text: _list[index].dateStart,
                isUseContent2Line: true,
                content2Subject: '이용시간',
                content2Text: _list[index].timeUse.replaceAll(".0", "분"),
                isUseContent3Line: true,
                content3Subject: '이용요금',
                content3Text: "${maskNumFormatter.format(_list[index].resultPrice)}원",
                voidCallback: () { Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageMyUseHistoryDetail(id: _list[index].id))); },
              ),
            )
          ],
        ),
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.history,
          msg: '이용내역이 없습니다.',
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}