import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_notification.dart';
import 'package:daligo_customer_app/config/config_form_validator.dart';
import 'package:daligo_customer_app/config/config_style.dart';
import 'package:daligo_customer_app/model/update_request.dart';
import 'package:daligo_customer_app/repository/repo_member.dart';
import 'package:daligo_customer_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import '../components/common/component_text_btn.dart';

class PageMemberUpdate extends StatefulWidget {
  const PageMemberUpdate({super.key});


  @override
  State<PageMemberUpdate> createState() => _PageMemberUpdateState();

  }

class _PageMemberUpdateState extends State<PageMemberUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  var maskLicenseNumber = MaskTextInputFormatter(
      mask: '##-##-######-##',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  Future<void> _memberUpdate(UpdateRequest updateRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().memberUpdate(updateRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '수정 완료',
        subTitle: '수정이 완료되었습니다.',
      ).call();

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '수정 실패',
        subTitle: '입력 내용을 확인해주세요.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          const ComponentAppbarActions(
            title: "회원 수정",
          ),
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.all(20),
            child: FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    decoration: formBoxDecoration,
                    padding: bodyPaddingAll,
                    child: FormBuilderTextField(
                      name: 'name',
                      decoration: StyleFormDecoration().getInputDecoration('이름'),
                      maxLength: 20,
                      keyboardType: TextInputType.text,
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: formErrorRequired),
                        FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                        FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                      ]),
                    ),
                  ),
                  const ComponentMarginVertical(),
                  Container(
                    decoration: formBoxDecoration,
                    padding: bodyPaddingAll,
                    child: FormBuilderTextField(
                      name: 'phoneNumber',
                      decoration: StyleFormDecoration().getInputDecoration('연락처'),
                      maxLength: 13,
                      inputFormatters: [maskPhoneNumber],
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: formErrorRequired),
                        FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)),
                        FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),
                      ]),
                    ),
                  ),
                  const ComponentMarginVertical(),
                  Container(
                    decoration: formBoxDecoration,
                    padding: bodyPaddingAll,
                    child: FormBuilderTextField(
                      name: 'licenseNumber',
                      decoration: StyleFormDecoration().getInputDecoration('면허증번호'),
                      maxLength: 15,
                      inputFormatters: [maskLicenseNumber],
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(errorText: formErrorRequired),
                        FormBuilderValidators.minLength(15, errorText: formErrorMinLength(15)),
                        FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
                      ]),
                    ),
                  ),
                  const ComponentMarginVertical(),
                  ComponentTextBtn('수정', () {
                    if(_formKey.currentState!.saveAndValidate()) {
                      UpdateRequest updateRequest = UpdateRequest( // LoginRequest에 입력받은 아이디, 비밀번호를 넘겨준다
                        _formKey.currentState!.fields['name']!.value,
                        _formKey.currentState!.fields['phoneNumber']!.value,
                        _formKey.currentState!.fields['licenseNumber']!.value,
                      );
                      _memberUpdate(updateRequest);
                    }
                  }),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
