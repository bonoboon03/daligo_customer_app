import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_no_contents.dart';
import 'package:daligo_customer_app/components/common/component_notification.dart';
import 'package:daligo_customer_app/components/common/component_response.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/config/config_style.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/functions/geo_check.dart';
import 'package:daligo_customer_app/model/ing_kick_board_response.dart';
import 'package:daligo_customer_app/model/kick_board_use_request.dart';
import 'package:daligo_customer_app/pages/page_main.dart';
import 'package:daligo_customer_app/repository/repo_kick_board_history.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:qr_code_dart_scan/qr_code_dart_scan.dart';


class PageIngKickBoard extends StatefulWidget {
  const PageIngKickBoard({super.key, required this.id});

  final int id;

  @override
  State<PageIngKickBoard> createState() => _PageIngKickBoardState();
}

class _PageIngKickBoardState extends State<PageIngKickBoard> {
  var maskNumFormatter = NumberFormat('###,###,###,###');

  final decoder = QRCodeDartScanDecoder(
    formats: [BarcodeFormat.QR_CODE],
  );

  IngKickBoardResponse _currentData =
  IngKickBoardResponse(0, "", 0, 0, "", "", 0, 0);
  Position? _currentPosition;

  void _startRoutine() async {
    await _getPosition().then((res) {
      _getUseData();
    });
  }

  Future<Position> _getPosition() async {
    try {
      Position position = await GeoCheck.determinePosition();

      setState(() {
        _currentPosition = position;
      });

      return position;
    } catch (e) {
      ComponentNotification(
        success: false,
        title: '위치 정보 없음',
        subTitle: e.toString(),
      ).call();

      return Future.error('킥보드 로딩 실패');
    }
  }

  Future<void> _endKickBoard() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(
        cancelFunc: cancelFunc,
      );
    });

    KickBoardUseRequest request = KickBoardUseRequest(
      _currentPosition!.latitude,
      _currentPosition!.longitude,
    );

    await RepoKickBoardHistory().putEnd(_currentData.historyId, request).then((res) {
      _getUseData();
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '이용 종료 실패',
        subTitle: '이미 이용 종료된 킥보드입니다.',
      ).call();
    });
  }

  Future<void> _getUseData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(
        cancelFunc: cancelFunc,
      );
    });

    await RepoKickBoardHistory()
        .getCurrentUsing()
        .then((res) => {
      BotToast.closeAllLoading(),
      setState(() {
        _currentData = res.data;
      })
    })
        .catchError((err) => {
      BotToast.closeAllLoading(),
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '고객센터로 문의해주세요.',
      ).call(),
    });
  }

  @override
  void initState() {
    super.initState();
    _startRoutine();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: colorPrimary,
        title: Image.asset(
          'assets/daligo_logo_white.png',
          fit: BoxFit.contain,
          height: sizeAppBarLogoHeight,
        ),
      ),
      body: _buildBody(),
    );
  }

  Size get preferredSize => const Size.fromHeight(sizeAppBarHeight);

  Widget _buildBody() {
    if (_currentPosition != null) {
      if (_currentData.historyId != 0) {
        return ListView(
          children: [
            Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      border: Border.all(color: colorLightGray),
                      borderRadius: BorderRadius.circular(buttonRadius),
                    ),
                    child: Column(
                      children: [
                        ComponentResponse(title: '모델명', content: _currentData.uniqueNumber),
                        const ComponentMarginVertical(),
                        ComponentResponse(title: '요금제', content: _currentData.priceBasis),
                        const ComponentMarginVertical(),
                        ComponentResponse(title: '기본요금', content: '${maskNumFormatter.format(_currentData.basePrice)}원'),
                        const ComponentMarginVertical(),
                        ComponentResponse(title: '분당요금', content: '${_currentData.perMinPrice}원'),
                        const ComponentMarginVertical(),
                        ComponentResponse(title: '시작위도', content: '${_currentData.startPosX}'),
                        const ComponentMarginVertical(),
                        ComponentResponse(title: '시작경도', content: '${_currentData.startPosY}'),
                        const ComponentMarginVertical(),
                        ComponentResponse(title: '시작시간', content: _currentData.dateStart),
                      ],
                    ),
                  ),
                  const ComponentMarginVertical(enumSize: EnumSize.mid),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: ComponentTextBtn(
                      '사용 종료',
                          () {
                        _endKickBoard();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      } else {
        return Container(
          padding: const EdgeInsets.all(20),
          child: Center(
            child: Column(
              children: [
                const Text(
                    '이용중인 킥보드가 없습니다.',
                    style: TextStyle(
                      fontSize: fontSizeSuper,
                    ),
                ),
                const ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn("홈으로 가기", () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMain()))),
                ),
              ],
            ),
          ),
        );
      }
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.gps_fixed,
          msg: '위치정보가 있어야만 사용가능합니다.',
        ),
      );
    }
  }
}