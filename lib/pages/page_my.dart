import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/pages/page_find_password.dart';
import 'package:daligo_customer_app/pages/page_member_update.dart';
import 'package:daligo_customer_app/pages/page_password_update.dart';
import 'package:flutter/material.dart';
import 'package:daligo_customer_app/components/common/component_appbar_logo.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/components/common/component_text_icon_full_btn.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';

class PageMy extends StatefulWidget {
  const PageMy({Key? key}) : super(key: key);

  @override
  _PageMyState createState() => _PageMyState();
}

class _PageMyState extends State<PageMy> with AutomaticKeepAliveClientMixin{
  String? memberName;

  Future<void> _getMemberName() async {
    String? resultName = await TokenLib.getMemberName();
    setState(() {
      memberName = resultName;
    });
  }

  Future<void> _logout() async {
    TokenLib.logout(context);
  }

  @override
  void initState() {
    super.initState();
    _getMemberName();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: ComponentAppbarLogo('logo.png', () {}),
      body: _buildBody(),
      drawer: const ComponentDrawer(),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Stack(
          children: [
            Column(
              mainAxisSize: MainAxisSize.min, // 크기만큼만 차지
              crossAxisAlignment: CrossAxisAlignment.stretch, // child widget의 width를 꽉 채운다
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  color: Colors.white,
                  child: Image.asset(
                    'login_bg.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      bottom: BorderSide(
                        width: 1,
                        color: colorLightGray,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: 210,
              left: 15,
              child: Text(
                '$memberName',
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: fontSizeMid,
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              left: 15,
              child: Row(
                children: [
                  Text(
                    '즐거운 킥보드 생활 되세요',
                    style: const TextStyle(
                      color: colorGray,
                      fontSize: fontSizeMicro,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        const ComponentMarginVertical(),
        ComponentTextIconFullBtn(Colors.white, Colors.black, Icons.lock, '로그아웃', () { _showLogoutDialog(); }),
        const ComponentMarginVertical(),
        ComponentTextIconFullBtn(Colors.white, Colors.black, Icons.restart_alt, '회원수정', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PageMemberUpdate()))),
        const ComponentMarginVertical(),
        ComponentTextIconFullBtn(Colors.white, Colors.black, Icons.restart_alt, '비밀번호수정', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => PagePasswordUpdate()))),
      ],
    );
  }
  void _showLogoutDialog() {
    showDialog(
      context: context,
      barrierDismissible: false, // 외부 화면을 눌러도 Dialog가 꺼지지 않게 함
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('로그아웃'),
          content: const Text('정말 로그아웃 하시겠습니까?'),
          actions: [
            ComponentTextBtn('확인',
                  () async { _logout(); },
            ),
            ComponentTextBtn('취소',
                  () { Navigator.of(context).pop(); },
              bgColor: Colors.black,
              borderColor: Colors.black,
            ),
          ],
        );
      },
    );
  }
  @override
  bool get wantKeepAlive => true;
}