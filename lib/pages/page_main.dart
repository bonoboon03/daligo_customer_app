import 'package:daligo_customer_app/components/common/component_appbar_logo.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/components/common/component_margin_horizon.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_text_icon_full_btn.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/pages/page_near_kick_board.dart';
import 'package:flutter/material.dart';

class PageMain extends StatefulWidget {
  const PageMain({super.key});

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  String? memberName;

  Future<void> _getMemberName() async {
    String? resultName = await TokenLib.getMemberName();
    setState(() {
      memberName = resultName;
    });
  }

  @override
  void initState() {
    super.initState();
    _getMemberName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarLogo(
        "daligo_logo_white.png",
        () {},
      ),
      body: _buildBody(context),
      drawer: const ComponentDrawer(),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    '$memberName',
                    style: const TextStyle(
                      fontSize: fontSizeBig,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const ComponentMarginHorizon(),
                  const Text(
                    " 님,",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                    ),
                  ),
                  const ComponentMarginHorizon(enumSize: EnumSize.mid),
                  const Text(
                    "안녕하세요!",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                    ),
                  ),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 200,
                color: Colors.white,
                child: Image.asset(
                  'assets/main_img.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              const Text(
                "오늘도 달리고와 함께 달려볼까요?",
                style: TextStyle(
                  fontSize: fontSizeBig,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const Text(
                "시작하기 버튼을 눌러서 시작해주세요.",
                style: TextStyle(
                  fontSize: fontSizeBig,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                child: ComponentTextIconFullBtn(
                  Colors.blue,
                  Colors.white,
                  Icons.play_arrow,
                  "시작하기",
                  () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const PageNearKickBoard())),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
