import 'package:flutter/material.dart';
import 'package:daligo_customer_app/components/common/component_margin_horizon.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';

class ComponentTextIconFullBtn extends StatelessWidget {
  final Color bgColor;
  final Color color;
  final IconData icon;
  final String text;
  final VoidCallback callback;

  const ComponentTextIconFullBtn(this.bgColor, this.color, this.icon, this.text, this.callback, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: bgColor,
          border: Border.all(color: colorLightGray),
          borderRadius: const BorderRadius.all(Radius.circular(5)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: color,
              size: 18,
            ),
            const ComponentMarginHorizon(enumSize: EnumSize.mid,),
            Text(
              text,
              style: TextStyle(
                color: color,
                fontSize: fontSizeMid,
              ),
            ),
          ],
        ),
      ),
      onTap: callback,
    );
  }
}
